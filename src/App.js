import logo from './logo.svg';
import './App.css';

import {Button, Row, Col, Form} from 'react-bootstrap'
import {useState} from 'react'

function App() {
  const [result, setResult] = useState(0)
  const [input1, setInput1] = useState('')
  const [input2, setInput2] = useState('')
  
  const resetValue = () => {
    setResult(0)
    setInput1('')
    setInput2('')
  }
  
  const computeResult = (ar) => {
    let compute = 0
    let val1 = parseInt(input1)
    let val2 = parseInt(input2)
    if (ar === "add") {
      compute= val1+val2
    }
    else if (ar === "sub") {
      compute= val1-val2
    }
    else if (ar === "mul") {
      compute= val1*val2
    }
    else if (val2===0){
      compute="Cannot divide by 0"
    }
    else {
      compute=val1/val2
    }
    
    
    setResult(compute)
  }
  
  return (
    <div className="App px-5">
    <h1>Calculator</h1>
    <h2>{result}</h2>
    <Row className="mb-4 mt-5">
    <Col>
    <Form.Control
    type="number"
    value={input1}
    onChange={(e) => setInput1(e.target.value) }
    />
    </Col>
    <Col>
    <Form.Control
    type="number"
    value={input2}
    onChange={(e) => setInput2(e.target.value) }
    />
    </Col>
    </Row>
    
    
    <Button onClick={()=> computeResult("add")}>Add</Button>
    <Button onClick={()=> computeResult("sub")}>Subtract</Button>
    <Button onClick={()=> computeResult("mul")}>Mutltiply</Button>
    <Button onClick={()=> computeResult("div")}>Divide</Button>
    <Button onClick={()=> resetValue()}>Reset</Button>
    
    </div>
    );
  }
  
  export default App;
